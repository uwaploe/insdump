// Insdump dumps the contents of an INS MessagePack archive file as
// raw binary
package main

import (
	"encoding/binary"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"runtime"

	ins "bitbucket.org/uwaploe/go-ins"
	must "bitbucket.org/uwaploe/go-must"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `Usage: insdump [options] infile outfile

Reads a MessagePack format INS records from INFILE and writes them to
OUTFILE in newline-delimited JSON format or raw binary.
`

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	appendMode = flag.Bool("append", false,
		"If true, append to OUTFILE rather than overwrite")
	useRaw = flag.Bool("raw", false,
		"If true, OUTFILE is in raw binary format")
	v2Fmt = flag.Bool("v2", false,
		"If true, decode the v2 INS data format")
)

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()

	if len(args) < 2 {
		flag.Usage()
		os.Exit(1)
	}

	var (
		fin   io.Reader
		fout  io.WriteCloser
		err   error
		flags int
	)

	fin, err = os.Open(args[0])
	if err != nil {
		log.Fatalf("Open %q failed: %v", args[0], err)
	}

	if *appendMode {
		flags = os.O_APPEND | os.O_CREATE | os.O_WRONLY
	} else {
		flags = os.O_CREATE | os.O_WRONLY
	}

	if args[1] == "-" {
		fout = os.Stdout
	} else {
		fout, err = os.OpenFile(args[1], flags, 0644)
		if err != nil {
			log.Fatalf("Open %q failed: %v", args[1], err)
		}
		defer fout.Close()
	}

	var rdr must.DataReader

	if *v2Fmt {
		rdr = must.NewInsUdV2Reader(fin)
	} else {
		rdr = must.NewInsUdReader(fin)
	}

	count := int(0)
	eol := []byte("\n")
	for rdr.Scan() {
		rec := rdr.Record()
		if *useRaw {
			if err := binary.Write(fout, ins.ByteOrder, rec); err != nil {
				log.Fatalf("Write error on record %d: %v", count, err)
			}
		} else {
			b, err := json.Marshal(rec)
			if err != nil {
				log.Fatalf("JSON encoding failed: %v", err)
			}
			fout.Write(b)
			_, err = fout.Write(eol)
			if err != nil {
				log.Fatalf("Write error on record %d: %v", count, err)
			}
		}
		count++
	}

	if err := rdr.Err(); err != nil {
		log.Fatalf("Read error on record %d: %v", count, err)
	}
	log.Printf("Read %d records", count)
}
