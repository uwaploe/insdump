# Dump INS Archive Records

Insdump reads a MuST INS archive file and dumps the records in
[newline-delimited JSON](http://ndjson.org) (ndJSON) or raw binary format.

## Installation

Visit the download page of this repository to obtain a compressed-tar
archive of the compiled binary for your OS (Linux, Windows, or MacOS).

## Usage

The `-help` option displays a usage message

``` shellsession
$ insdump -help
Usage: insdump [options] infile outfile

Reads a MessagePack format INS records from INFILE and writes them to
OUTFILE in newline-delimited JSON format or raw binary.
  -append
    	If true, append to OUTFILE rather than overwrite
  -old
    	If true, decode the old INS data format
  -raw
    	If true, OUTFILE is in raw binary format
  -version
    	Show program version information and exit
```

Convert a file to njJSON.

``` shellsession
$ insdump -old=true ins-20190806-22.mpk ins.ndjson
2019/08/19 12:34:05 Read 180091 records
$ head -n 2 ins.ndjson
{"Tsec":1565128801,"Tnsec":750000000,"ImuOffset":4138985,"Gyro":[0.1777,0.1678,0.0295],"Accel":[0.0527,0.3686,9.8365],"Usw":0,"Heading":28.698,"Pitch":1.885,"Roll":5.01,"Lat":46.992774515,"Lon":-119.994773685,"Alt":2977.849,"E":477.96,"N":-279.67,"U":11.34,"Gnss_info1":6,"Gnss_info2":9,"Svs":0,"New_gps":0}
{"Tsec":1565128801,"Tnsec":770000000,"ImuOffset":4138329,"Gyro":[0.0476,0.294,-0.1557],"Accel":[0.0719,0.2668,9.8099],"Usw":0,"Heading":28.7,"Pitch":1.885,"Roll":5.01,"Lat":46.992724234,"Lon":-119.994647705,"Alt":2978.076,"E":477.97,"N":-279.68,"U":11.34,"Gnss_info1":6,"Gnss_info2":9,"Svs":0,"New_gps":1}
$
```

Convert a file to binary.

``` shellsession
$ insdump -old=true -raw=true ins-20190806-22.mpk ins.bin
2019/08/19 12:35:24 Read 180091 records
$ ls -l ins.bin
-rw-r--r-- 1 mike 21971102 2019-08-19 12:35 ins.bin
$
```
