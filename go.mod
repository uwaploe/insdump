module bitbucket.org/uwaploe/insdump

require (
	bitbucket.org/uwaploe/go-ins v1.5.0
	bitbucket.org/uwaploe/go-must v0.8.0
	google.golang.org/appengine v1.6.2 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)

go 1.13
